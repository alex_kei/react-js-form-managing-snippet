import SimpleInput from './components/SimpleInput';

export default function App() {
  return (
    <div className="app">
      <SimpleInput />
    </div>
  );
};
